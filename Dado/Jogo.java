package com.company.dia103.Dado;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Jogo {

    private int faceSorteada;
    private Dado dado;
    private Random gerador;
    private List<Integer> valores;

    public void preencherFaces(){
        IO entrada = new IO();
        System.out.print("Quantas faces terá o dado? ");
        int quant = entrada.getIn().nextInt();
        this.dado = new Dado(quant);
    }

    public void sorteador(){
        this.gerador = new Random();
        this.setFaceSorteada(gerador.nextInt(this.dado.getQuantFaces()-1)+1);
    }

    public void sorteadorComSoma(){
        this.valores = new ArrayList<>();
        int soma = 0;
        for (int i=0;i<3;i++){
            sorteador();
            valores.add(this.getFaceSorteada());
            soma+=this.getFaceSorteada();
        }
        valores.add(soma);
        IO.valoresSoma(valores);
    }

    public void sorteadorMultiplasRodadas(){
        for (int i=0;i<3;i++){
            sorteadorComSoma();
        }
    }

    public int getFaceSorteada() {
        return faceSorteada;
    }

    public void setFaceSorteada(int faceSorteada) {
        this.faceSorteada = faceSorteada;
    }
}
