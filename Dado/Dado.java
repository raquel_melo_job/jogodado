package com.company.dia103.Dado;

public class Dado {
    private int quantFaces;

    public Dado(int quantFaces) {
        this.quantFaces = quantFaces;
    }

    public int getQuantFaces() {
        return quantFaces;
    }

    public void setQuantFaces(int quantFaces) {
        this.quantFaces = quantFaces;
    }
}
