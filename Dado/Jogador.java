package com.company.dia103.Dado;

import java.util.Random;

public class Jogador {
    private Jogo jogo;
    private IO saida;

    public Jogador() {
        this.jogo = new Jogo();
        this.saida = new IO();
    }

    public void rodada1(){
        Dado dado = new Dado(6);
        Random gerador = new Random();
        this.saida.setResultado(gerador.nextInt(dado.getQuantFaces()-1)+1);
        this.saida.valorSorteado();
    }

    public void rodada2(){
        this.jogo.preencherFaces();
        this.jogo.sorteador();
        this.saida.setResultado(jogo.getFaceSorteada());
        this.saida.valorSorteado();
    }

    public void rodada3(){
        this.jogo.preencherFaces();
        this.jogo.sorteadorComSoma();
    }

    public void rodada4(){
        this.jogo.preencherFaces();
        this.jogo.sorteadorMultiplasRodadas();
    }
}
