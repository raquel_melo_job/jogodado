package com.company.dia103.Dado;

import java.util.List;
import java.util.Scanner;

public class IO {
    private Scanner in;
    private int resultado;


    public IO() {
        this.in = new Scanner(System.in);
    }

    public Scanner getIn() {
        return in;
    }

    public void setIn(Scanner in) {
        this.in = in;
    }

    public void valorSorteado() {
        System.out.println("O número sorteado é: "+ this.resultado);
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public static void valoresSoma(List<Integer> valores) {
        System.out.println(valores);
    }
}
